//
//  DownloadImageTests.swift
//  PK_MovieListTests
//
//  Created by Ananth Chepuri on 06/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import XCTest
import UIKit
@testable import PK_MovieList

class DownloadImageTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testDownloadImage() {
        DownloadImage.downloadImage(url: "")
        DownloadImage.downloadImage(url: "https://is2-ssl.mzstatic.com/image/thumb/Video113/v4/0f/26/b6/0f26b625-ef90-482f-6f4b-37e24ea6ca5c/pr_source.jpg/113x170bb.png")
        DownloadImage.downloadImage(url: "https://is2-ssl.mzstatic.com/ima/thumb/Video113/v4/0f/26/b6/0f26b625-ef90-482f-6f4b-37e24ea6ca5c/pr_source.jpg/113x170bb.png")

    }
}
