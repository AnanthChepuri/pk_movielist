//
//  MovieListTests.swift
//  PK_MovieListTests
//
//  Created by Ananth Chepuri on 05/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import XCTest
import UIKit
@testable import PK_MovieList

class MovieListTests: XCTestCase {

    var movieList: MovieListViewController?
    var feedsManager = FeedsManager()
    var feedsModel: [Entry] = []
    var error: Error?
    var originalArray: [Entry] = []

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let movieVC = storyboard.instantiateViewController(withIdentifier: "MovieListViewController") as? MovieListViewController {
              self.movieList = movieVC
        } else {
            XCTAssert(false, "MovieViewController failed to instantiate")
        }
        movieList?.view.setNeedsLayout()
        movieList?.view.layoutSubviews()
        movieList?.viewDidLoad()

        self.movieList?.feedsManager = feedsManager
        self.movieList?.feedsModel = feedsModel
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testSearchWithString() {
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            movieList.movieListSearch.text = ""
            movieList.searchBar(movieList.movieListSearch, textDidChange: "")
        }
    }

    func testDidUpdateFeeds() {
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            movieList.didUpdateFeeds(movieList.feedsManager, feedsModel: movieList.feedsModel)
        }
    }

    func testDidFailWithError() {
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            if let err = self.error {
                movieList.didFailWithError(error: err)
            }
        }
    }

    func testSearchBarCancelButtonClicked() {
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            XCTAssert("" == movieList.movieListSearch.text)
            XCTAssert(false == movieList.movieListSearch.resignFirstResponder())
        }
    }

    func testSearchBarTextDidChange() {
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            movieList.searchBar(movieList.movieListSearch, textDidChange: "Action")
            movieList.searchBar(movieList.movieListSearch, textDidChange: "")
        }
    }

    func testTableViewHeightForRowAtIndexPath() {
        let indexPath: IndexPath = IndexPath(row: 1, section: 1)
        XCTAssert(self.movieList != nil)
        if let movieList = self.movieList {
            XCTAssert(120 == movieList.tableView(movieList.movieListTableView, heightForRowAt: indexPath))
        }
    }
}
