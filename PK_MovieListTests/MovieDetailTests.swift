//
//  MovieDetailTests.swift
//  PK_MovieListTests
//
//  Created by Ananth Chepuri on 06/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import XCTest
import UIKit
@testable import PK_MovieList

class MovieDetailTests: XCTestCase {

    var movieDetail: MovieDetailViewController?
    var feed: Entry?
    var imageData: Data?
    var tableView: UITableView!

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let movieDetl = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
              self.movieDetail = movieDetl
        } else {
            XCTAssert(false, "MovieViewController failed to instantiate")
        }
        movieDetail?.loadViewIfNeeded()

        movieDetail?.view.setNeedsLayout()
        movieDetail?.view.layoutSubviews()
        movieDetail?.viewDidLoad()

//        XCTAssertNotNil(movieDetail?.tableView, "Controller should have a tableview")

//        self.movieDetail?.tableView.delegate = self
//        self.movieDetail?.tableView.dataSource = self
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

    func testNumberOfRowsInSection() {
        XCTAssert(self.movieDetail != nil)
        if let movieDetl = self.movieDetail {
            let result = movieDetl.tableView(movieDetl.tableView, numberOfRowsInSection: 1)
            XCTAssert(1 == result)
        }
    }

    func testcellForRowAtIndixPath() {
        XCTAssert(self.movieDetail != nil)
        if let movieDetl = self.movieDetail {
        }

    }
}
