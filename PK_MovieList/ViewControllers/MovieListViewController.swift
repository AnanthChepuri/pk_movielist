//
//  ViewController.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 03/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import SplunkMint

class MovieListViewController: UIViewController {

    @IBOutlet weak var movieListTableView: UITableView!
    @IBOutlet weak var movieListSearch: UISearchBar!

    var feedsManager = FeedsManager()
    var feedsModel: [Entry] = []
    var originalArray: [Entry] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        feedsManager.delegate = self
        feedsManager.fetchFeeds()
        movieListSearch.delegate = self
        movieListSearch.showsCancelButton = true
    }
}

extension MovieListViewController: FeedsManagerDelegate {

    func didUpdateFeeds(_ feedsManager: FeedsManager, feedsModel: [Entry]) {
        DispatchQueue.main.async {
            self.feedsModel = feedsModel
            self.originalArray = feedsModel
            self.movieListTableView.reloadData()
        }
    }

    func didFailWithError(error: Error) {
        print(error)
    }
}

extension MovieListViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return feedsModel.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "LabelCell", for: indexPath) as? FeedsTableCell {
            let feedData = self.feedsModel[indexPath.row]
            cell.feedData(feed: feedData)
            return cell
        }
        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let feedData = self.feedsModel[indexPath.row]

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let movieDetailVC = storyboard.instantiateViewController(withIdentifier: "MovieDetailViewController") as? MovieDetailViewController {
            movieDetailVC.feed = feedData
            self.navigationController?.pushViewController(movieDetailVC, animated: true)
        }
    }
}

extension MovieListViewController: UISearchBarDelegate {

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            feedsModel = self.originalArray
            self.movieListTableView.reloadData()
            return
        }
        feedsModel = self.originalArray
        let searchText = searchBar.text
        let arr = feedsModel.filter { (entry) -> Bool in
            return entry.title?.label?.searchWithString(searchText: searchText) ?? false ||
                entry.category?.attributes?.label?.searchWithString(searchText: searchText) ?? false
        }
        feedsModel = arr
        self.movieListTableView.reloadData()
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        self.movieListSearch.text = ""
        self.movieListSearch.resignFirstResponder()
        feedsModel = originalArray
        self.movieListTableView.reloadData()
    }
}

extension String {

    func searchWithString(searchText: String?) -> Bool {
        guard let searchTitle = searchText else {
            return false
        }
        if self.contains(searchTitle) {
            return true
        }
        return false
    }
}
