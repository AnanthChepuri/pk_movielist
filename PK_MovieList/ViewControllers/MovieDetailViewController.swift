//
//  MovieDetailViewController.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 04/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit

class MovieDetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var feed: Entry?
    var imageData: Data?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension MovieDetailViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: "FeedDetailCell", for: indexPath) as? FeedDetailCell {
            if let feedData = self.feed {
                cell.feedData(feed: feedData)
            }
            return cell
        }
        return UITableViewCell()
    }
}
