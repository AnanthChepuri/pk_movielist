//
//  FeedsTableCell.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 03/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import SplunkMint

class FeedsTableCell: UITableViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    var imageData: Data?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.titleLabel.sizeToFit()
    }

    func feedData(feed: Entry) {

        self.titleLabel.text = feed.title?.label
        self.categoryLabel.text = feed.category?.attributes?.term
        self.releaseDateLabel.text = feed.releaseDate?.attributes.label

        if let imageData = self.imageData, self.logoImageView.image == nil {
            self.logoImageView.image = UIImage(data: imageData)
        } else {
            DispatchQueue.main.async {
                self.logoImageView.image = DownloadImage.downloadImage(url: feed.imImage?[2].label ?? "")
            }
        }
    }
}
