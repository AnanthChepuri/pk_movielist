//
//  DownloadImage.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 06/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import SplunkMint

class DownloadImage {

    static func downloadImage(url: String) -> UIImage? {

        if let urlStr = URL(string: url) {
            do {
               let data = try Data(contentsOf: urlStr)
                Mint.sharedInstance()?.logEvent(withName: "Image downloaded")
                return UIImage(data: data)
            } catch {
                Mint.sharedInstance()?.logEvent(withName: "Image downloading failed with error \(error)")
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
