//
//  FeedDetailCell.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 04/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import SplunkMint

class FeedDetailCell: UITableViewCell {

    @IBOutlet weak var movieTitleImage: UIImageView!
    @IBOutlet weak var movieTitleLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var summaryLabel: UILabel!
    var imageData: Data?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func feedData(feed: Entry) {

        self.movieTitleLabel.text = feed.title?.label
        self.categoryLabel.text = feed.category?.attributes?.term
        self.releaseDateLabel.text = feed.releaseDate?.attributes.label
        self.summaryLabel.text = feed.summary?.label
        self.summaryLabel.sizeToFit()
        self.summaryLabel.contentMode = .topLeft

        if let imageData = self.imageData, self.movieTitleImage.image == nil {
            self.movieTitleImage.image = UIImage(data: imageData)
        } else {
            DispatchQueue.main.async {
                self.movieTitleImage.image = DownloadImage.downloadImage(url: feed.imImage?[2].label ?? "")
            }
        }
    }
}
