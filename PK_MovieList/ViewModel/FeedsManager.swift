//
//  FeedsManager.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 03/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

protocol FeedsManagerDelegate {
    func didUpdateFeeds(_ feedsManager: FeedsManager, feedsModel: [Entry])
    func didFailWithError(error: Error)
}

struct FeedsManager {

    let feedsURL = "https://itunes.apple.com/us/rss/topmovies/limit=50/json"
    var delegate: FeedsManagerDelegate?

    func fetchFeeds() {
        performRequest(with: feedsURL)
    }

    func performRequest(with urlString: String) {
        if let url  = URL(string: urlString) {
            NetworkService.fetchMovieList(url, Feed.self) { result in
                switch result {
                case .success(let data):
                    self.delegate?.didUpdateFeeds(self, feedsModel: data.feed.entry!)
                case .failure(let error):
                    self.delegate?.didFailWithError(error: error)
                }
            }
        }
    }
}
