//
//  AppDelegate.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 03/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import UIKit
import ADEUMInstrumentation
import SplunkMint

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        //Integarting AppDynamics
        ADEumInstrumentation.initWithKey("AD-AAB-AAV-FZP")

        let config = ADEumAgentConfiguration(appKey: "AD-AAB-AAV-FZP")
        // Configure the iOS Agent to report the metrics and screenshots

        // to the SaaS EUM Server in Americas

        config.collectorURL = "https://col.eum-appdynamics.com"
        config.screenshotURL = "https://image.eum-appdynamics.com"
        ADEumInstrumentation.initWith(config)
        
        //  Integarting SplunkMint
        Mint.sharedInstance()?.initAndStartSession(withAPIKey: "b368a1a0")
        Mint.sharedInstance()?.enableLogging(true)
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
}
