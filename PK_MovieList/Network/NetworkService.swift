//
//  NetworkService.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 04/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation
import SplunkMint

class NetworkService {

    public enum NetworkError: Error {
        case domainError
        case decodingError
        case noDataError
    }

    static func fetchMovieList<T: Decodable>(_ requestURL: URL,
                                         _ modelType: T.Type,
                                         completion: @escaping (Result<T, NetworkError>) -> Void) {
        let session = URLSession.shared
        let request = URLRequest(url: requestURL)

        session.dataTask(with: request) { (data, response, error) in
            if let _ = error {
                completion(.failure(.domainError))
            }
            if let response = response {
                print(response)
            }
            if let data = data {
                do {
                    let jsonDecoder = JSONDecoder()
                    let responseModel = try jsonDecoder.decode(T.self, from: data)
                    completion(.success(responseModel))
                    Mint.sharedInstance()?.logEvent(withName: "MoviesList received")

                } catch {
                    completion(.failure(.decodingError))
                    Mint.sharedInstance()?.logEvent(withName: "MoviesList receiving failed with error \(error)")
                    print(error)
                }
            }
        }.resume()
    }
}
