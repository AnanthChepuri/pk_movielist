//
//  Feed.swift
//  PK_MovieList
//
//  Created by Ananth Chepuri on 03/03/20.
//  Copyright © 2020 Ananth Chepuri. All rights reserved.
//

import Foundation

struct Feed: Codable {
    let feed: Feeds
}

struct Feeds: Codable {
    let entry: [Entry]?
    let title: Title?
}

struct Entry: Codable {
    let imImage: [ImImage]?
    let title: Title?
    let category: Category?
    let releaseDate: ReleaseDate?
    let summary: Summary?
    enum CodingKeys: String, CodingKey {
        case releaseDate = "im:releaseDate"
        case imImage = "im:image"
        case title
        case category
        case summary
    }
}

struct ImImage: Codable {
    let label: String
}

struct Summary: Codable {
    let label: String
}

struct Category: Codable {
    let attributes: Attributes?
}

struct Attributes: Codable {
    let term: String?
    let label: String?
}

struct Title: Codable {
    let label: String?
}

struct ReleaseDate: Codable {
    let label: String
    let attributes: Attributes
}
